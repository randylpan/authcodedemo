## 模擬登入資料

 - 身分證帳號:user
 - 密碼:123456
 - 信用卡號:A123456
 - 信用卡後三碼:123
 
 

## 開發流程

 1. 自訂頁面放在/resource/templates底下(login.html(登入頁), approval.html(授權頁))
 2. 自訂filter做前置資料處理,根據不同核身方式組成UsernamePasswordAuthenticationToken的子類
 3. 實作AbstractUserDetailsAuthenticationProvider類別,根據不同核身方式實作身份驗證
 4. 講自訂filter及provider實現類設定至WebSecurityConfigurerAdapter子類
 5. 新增Controller類別分別導向login及approval頁面

## 程式碼閱讀流程

 1. login.html
 2. LoginController
 3. authentication package下的AuthenticationToken
 4. filters package下的CustomUserFilter
 5. provider package下的provider實作
 6. config package下的DemoWebSecurityConfig
 7. approval.html
 8. ApprovalController

## 請求流程

 1. 訪問[http://localhost:8080/oauth/authorize?response_type=code&client_id=client&redirect_uri=https://www.google.com](http://localhost:8080/oauth/authorize?response_type=code&client_id=client&redirect_uri=https://www.google.com) 
 2. 登入頁面可選擇
 身分證驗證(身分證號+密碼+身分證驗證單選鈕),
 信用卡驗證(信用卡號+密碼+信用卡後三碼+信用卡驗證單選鈕)
 4. 選擇要授權的項目後會導向https://www.google.coｍ/?**code=sdfjjf**   code為隨機產生
 5. postman訪問  /oauth/token(http post)  Authorization欄位選Basic Auth,username password分別填入client/123456, request body如下

		grant_type: "authorization_code"
	    code: "JG2iLD" 由步驟三取得
	    redirect_uri: "https://www.google.com"
	    client_id: "client"
