package com.authcode.demo.grants;

import com.thinkpower.tsmp.art.apim.security.support.ifs.SMSOTPGrant;
import com.thinkpower.tsmp.art.apim.security.support.pojo.TsmpTokenRequest;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Map;

@Component
public class CustomSMSOTPGranter implements SMSOTPGrant {

    /* [static] field */

    /* [static] */

    /* [static] method */

    /* [instance] field */

    /* [instance] constructor */

    /* [instance] method */


    @Override
    public TsmpTokenRequest grant(TsmpTokenRequest tsmpTokenRequest) {
        Map<String, String> requestParameters = tsmpTokenRequest.getRequestParameters();

        //取得OTP值,欄位可自訂
        String otpCode = requestParameters.get("otp_code");
        if (StringUtils.isEmpty(otpCode)) {
            throw new OAuth2Exception("otp code can\\'t be empty");
        }

        // 模擬OTP驗證
        if (!"123456".equals(otpCode)) {
            throw new OAuth2Exception("otp code error");
        }

        //如驗證結果有特定回傳值可加至TsmpTokenRequest，後續可在TokenEnhancer中使用
        return tsmpTokenRequest;
    }
    /* [instance] getter/setter */
}
