package com.authcode.demo.filters.user;

import com.authcode.demo.authentication.IBUsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 繼承 {@link UsernamePasswordAuthenticationFilter} 並實作attemptAuthentication 做前置處理
 */
public class CustomUserFilter extends UsernamePasswordAuthenticationFilter {

    /* [static] field */

    /* [static] */

    /* [static] method */

    /* [instance] field */

    /* [instance] constructor */


    /* [instance] method */

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {

        //如有圖形驗證碼驗證可在這先驗證再做後續身份驗證


        //頁面上需有username,password兩欄位,如沒有 則自行轉換
        String username = obtainUsername(request);
        String password = obtainPassword(request);

        UsernamePasswordAuthenticationToken authRequest;

        //將身分證字號當成username
        String id = request.getParameter("ib");
        authRequest = new IBUsernamePasswordAuthenticationToken(id.trim(), password);


        // Allow subclasses to set the "details" property
        setDetails(request, authRequest);

        //做後續spring security user驗證流程 不用改
        return this.getAuthenticationManager().authenticate(authRequest);
    }


    /* [instance] getter/setter */
}
