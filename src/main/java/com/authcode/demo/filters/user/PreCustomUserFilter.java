package com.authcode.demo.filters.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.authcode.demo.authentication.PreIBUsernamePasswordAuthenticationToken;

public class PreCustomUserFilter extends AbstractAuthenticationProcessingFilter {
	
	public static final String SPRING_SECURITY_FORM_USERNAME_KEY = "username";
	public static final String SPRING_SECURITY_FORM_PASSWORD_KEY = "password";

	private String usernameParameter = SPRING_SECURITY_FORM_USERNAME_KEY;
	private String passwordParameter = SPRING_SECURITY_FORM_PASSWORD_KEY;
	
	private RequestCache requestCache = new HttpSessionRequestCache();

    /* [static] field */

    /* [static] */

    /* [static] method */

    /* [instance] field */

    /* [instance] constructor */


    /* [instance] method */
	
	public PreCustomUserFilter() {
		super(new AntPathRequestMatcher("/otp_check", "POST"));
	}

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        //頁面上需有username,password兩欄位,如沒有 則自行轉換
        String username = obtainUsername(request);
        String password = obtainPassword(request);

        UsernamePasswordAuthenticationToken authRequest;

        //將身分證字號當成username
        String id = request.getParameter("ib");
        String phoneNumber = request.getParameter("phoneNum");
        String otp = request.getParameter("otp");
        authRequest = new PreIBUsernamePasswordAuthenticationToken(id.trim(), password, phoneNumber, otp);

        // Allow subclasses to set the "details" property
        setDetails(request, authRequest);
        requestCache.saveRequest(request, response);

        //做後續spring security user驗證流程 不用改
        return this.getAuthenticationManager().authenticate(authRequest);
    }

    protected String obtainPassword(HttpServletRequest request) {
		return request.getParameter(passwordParameter);
	}
    
    protected String obtainUsername(HttpServletRequest request) {
		return request.getParameter(usernameParameter);
	}
    
    protected void setDetails(HttpServletRequest request,
			UsernamePasswordAuthenticationToken authRequest) {
		authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
	}

    /* [instance] getter/setter */
}
