package com.authcode.demo.authentication;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 * 將身分證驗證欄位組成UsernamePasswordAuthenticationToken子類供後續驗證使用
 * 會將身分證字號當成username,因UsernamePasswordAuthenticationToken需要username,
 * password兩必要欄位
 */
public class PreIBUsernamePasswordAuthenticationToken extends UsernamePasswordAuthenticationToken {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Object phoneNumber;
	
	private Object otpCode;

	public PreIBUsernamePasswordAuthenticationToken(Object principal, Object credentials, Object phoneNumber, Object otpCode) {
		super(principal, credentials);
		this.phoneNumber = phoneNumber;
		this.otpCode = otpCode;
	}

	public PreIBUsernamePasswordAuthenticationToken(Object principal, Object credentials,
			Collection<? extends GrantedAuthority> authorities, Object phoneNumber, Object otpCode) {
		super(principal, credentials, authorities);
		this.phoneNumber = phoneNumber;
		this.otpCode = otpCode;
	}

	public Object getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Object phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Object getOtpCode() {
		return otpCode;
	}

	public void setOtpCode(Object otpCode) {
		this.otpCode = otpCode;
	}

}
