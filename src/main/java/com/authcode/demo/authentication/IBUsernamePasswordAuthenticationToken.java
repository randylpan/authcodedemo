package com.authcode.demo.authentication;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 *  將身分證驗證欄位組成UsernamePasswordAuthenticationToken子類供後續驗證使用
 *  會將身分證字號當成username,因UsernamePasswordAuthenticationToken需要username, password兩必要欄位
 */
public class IBUsernamePasswordAuthenticationToken extends UsernamePasswordAuthenticationToken {

    public IBUsernamePasswordAuthenticationToken(Object principal, Object credentials) {
        super(principal, credentials);
    }

    public IBUsernamePasswordAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }
}
