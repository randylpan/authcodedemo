package com.authcode.demo.config;

import com.authcode.demo.filters.user.CustomUserFilter;
import com.authcode.demo.filters.user.PreCustomUserFilter;
import com.authcode.demo.handler.PreSavedRequestAwareAuthenticationSuccessForwardHandler;
import com.authcode.demo.providers.IBUserAuthenticationProvider;
import com.authcode.demo.providers.PreIBUserAuthenticationProvider;
import com.thinkpower.tsmp.sdk.art.ss.SavedRequestAwareAuthenticationSuccessForwardHandler;
import com.thinkpower.tsmp.sdk.art.ss.TsmpWebSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@Order(2)
public class DemoWebSecurityConfig extends TsmpWebSecurityConfig {

    @Autowired
    private UserDetailsService mockUserDetailsService;

    /**若登入路徑不為/login則可複寫此變數**/
    protected String DEFAULT_LOGIN_URL = "/login";
    /* [instance] constructor */

    /* [instance] method */

    /**
     * 此處設定當訪問  "/oauth/authorize","/oauth/confirm_access" 兩路徑時需要先驗證是否登入,如未登入則導向/login
     * 並加入客製化Filter在UsernamePasswordAuthenticationFilter前執行
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /** 複寫此方法皆需先呼叫一次父類別 **/
        super.configure(http);

        http
                //因開啟resource server設定，因此需要對網頁路徑特別額外設定，避免被resource server判定需要token才能訪問
                .requestMatchers().antMatchers("/oauth/authorize",
                "/oauth/confirm_access",
                "/tsp/revoke",
                "/ib/revoke",
                "/otp",
                "/revoke",
                DEFAULT_LOGIN_URL,
                "/identity",
                "/otpcheck",
                "/css/*",
                "/js/*",
                "/pre-login",
                "/check_input",
                "/otp_check").and()
                //並支援csrf防護
                .csrf().and()
                //此設定判別以下路徑需要登入
                .authorizeRequests().antMatchers("/oauth/authorize",
                "/oauth/confirm_access",
                "/tsp/revoke",
                "/ib/revoke",
                "/revoke",
                "/identity",
                "/otpcheck").authenticated().and()
                //此設定判別以下路徑不需要登入
                .authorizeRequests().antMatchers(DEFAULT_LOGIN_URL,"/css/*","/js/*").permitAll().and()
                .formLogin().loginPage(DEFAULT_LOGIN_URL).permitAll();
        http.addFilterBefore(customUserFilter(), UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(preCustomUserFilter(), UsernamePasswordAuthenticationFilter.class);

    }

    /**
     * 加入Provider實作類別
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(ibAuthenticationProvider());
        auth.authenticationProvider(preIBAuthenticationProvider());
    }



    public CustomUserFilter customUserFilter() throws Exception {
        CustomUserFilter filter = new CustomUserFilter();
        filter.setAuthenticationManager(authenticationManagerBean());
        /** 部署至TSMP時才需打開 */
        filter.setAuthenticationSuccessHandler(new SavedRequestAwareAuthenticationSuccessForwardHandler());

        return filter;
    }
    
    public PreCustomUserFilter preCustomUserFilter() throws Exception {
        PreCustomUserFilter filter = new PreCustomUserFilter();
        filter.setAuthenticationManager(authenticationManagerBean());
        /** 部署至TSMP時才需打開 */
        filter.setAuthenticationSuccessHandler(new PreSavedRequestAwareAuthenticationSuccessForwardHandler());

        return filter;
    }

    @Bean
    public AuthenticationProvider ibAuthenticationProvider(){
        IBUserAuthenticationProvider provider = new IBUserAuthenticationProvider();
        provider.setUserDetailsService(mockUserDetailsService);
        return provider;
    }
    
    @Bean
    public AuthenticationProvider preIBAuthenticationProvider(){
        PreIBUserAuthenticationProvider provider = new PreIBUserAuthenticationProvider();
        provider.setUserDetailsService(mockUserDetailsService);
        return provider;
    }


    /* [instance] getter/setter */
}
