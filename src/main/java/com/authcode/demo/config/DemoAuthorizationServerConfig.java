package com.authcode.demo.config;

import com.thinkpower.tsmp.sdk.TsmpSDKAutoConfig;
import com.thinkpower.tsmp.sdk.art.ss.OnLocalAuthorizationServerConfig;
import com.thinkpower.tsmp.util.spring.ex.tool.OnLocalLaunchCondition;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

/**
 * 此處會由tsmp控管  無須理會
 */
@Configuration
@EnableAuthorizationServer
@Import(TsmpSDKAutoConfig.class)
@Conditional(OnLocalLaunchCondition.class)
public class DemoAuthorizationServerConfig extends OnLocalAuthorizationServerConfig {

}
