package com.authcode.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * 因為不存User資料,但spring security驗證過程中需要取得User資料所以模擬回傳一個假的User
 */
@Service
public class MockUserDetailsService implements UserDetailsService {

    /* [static] field */

    /* [static] */

    /* [static] method */

    /* [instance] field */
    @Autowired
    private PasswordEncoder passwordEncoder;

    /* [instance] constructor */

    /* [instance] method */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return new User(username,passwordEncoder.encode(""),
                true,true,true,true,
                AuthorityUtils.commaSeparatedStringToAuthorityList("member"));
    }

    /* [instance] getter/setter */
}
