package com.authcode.demo.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.util.StringUtils;

public class PreSavedRequestAwareAuthenticationSuccessForwardHandler extends SimpleUrlAuthenticationSuccessHandler {

	/* [static] field */

	/* [static] */

	/* [static] method */

	/* [instance] field */

	private final Logger logger = LoggerFactory.getLogger(getClass()); // slf4j

	private RequestCache requestCache = new HttpSessionRequestCache();

	/* [instance] constructor */

	/* [instance] method */

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws ServletException, IOException {
		SavedRequest savedRequest = requestCache.getRequest(request, response);

		if (savedRequest == null) {
			logger.info("saveRequest == null");
			super.onAuthenticationSuccess(request, response, authentication);

			return;
		}
		String targetUrlParameter = getTargetUrlParameter();
		if (isAlwaysUseDefaultTargetUrl()
				|| (targetUrlParameter != null && StringUtils.hasText(request.getParameter(targetUrlParameter)))) {
			requestCache.removeRequest(request, response);
			super.onAuthenticationSuccess(request, response, authentication);

			return;
		}

		clearAuthenticationAttributes(request);

		// Use the DefaultSavedRequest URL
		String dcName = (String) request.getAttribute("tsmp.request.process.dc.name");
		String moduleName = (String) request.getAttribute("tsmp.request.process.module.name");
		DefaultSavedRequest defaultSavedRequest = (DefaultSavedRequest) savedRequest;
		String requestURI = defaultSavedRequest.getRequestURI();
		String queryString = defaultSavedRequest.getQueryString();
		String targetUrl = dcName + "/" + moduleName + requestURI;
		if (queryString != null) {
			targetUrl = targetUrl + "?" + queryString;
		}
		logger.info("dcName: " + dcName);
		logger.info("moduleName: " + moduleName);
		logger.info("targetUrl: " + targetUrl);
		logger.info("Redirecting to DefaultSavedRequest Url: " + targetUrl);
		getRedirectStrategy().sendRedirect(request, response, targetUrl);
	}

	public void setRequestCache(RequestCache requestCache) {
		this.requestCache = requestCache;
	}

}
