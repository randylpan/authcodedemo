package com.authcode.demo.controller;

import com.thinkpower.tsmp.apim.ifs.TsmpApprovalService;
import com.thinkpower.tsmp.apim.vo.TsmpScope;
import com.thinkpower.tsmp.smd.ns.ifs.NodeArtSupporter;
import com.thinkpower.tsmp.smd.ns.ifs.vo.TsmpClient;
import com.thinkpower.tsmp.smd.tg.ifs.ho.AuthorizationRevoke;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class RevokeController {

    @Autowired
    private TsmpApprovalService tsmpApprovalService;

    @Autowired
    private NodeArtSupporter nodeArtSupporter;

    @Autowired
    private AuthorizationRevoke authorizationRevoke;

    @GetMapping("/tsp/revoke")
    public ModelAndView tspRevoke(@RequestParam String clientId) {
        ModelAndView view = new ModelAndView();
        view.setViewName("tsp_revoke");
        TsmpClient tsmpClient = nodeArtSupporter.getClientById(clientId);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userName = auth.getName();

        List<TsmpScope> tsmpScopes = tsmpApprovalService.getApprovedByUserIdAndClientId(userName, clientId);
        tsmpScopes.forEach(tsmpScope -> {
            tsmpScope.setExpireDate(expireDate(tsmpScope.getAllowDays(), tsmpScope.getLastUpdateAt()));
            tsmpScope.setAllowDays(secToDay(tsmpScope.getAllowDays()));
        });
        view.addObject("tsmpClient", tsmpClient);
        view.addObject("tsmpScopes", tsmpScopes);
        return view;
    }

    @GetMapping("/ib/revoke")
    public ModelAndView ibRevoke() {
        ModelAndView view = new ModelAndView();
        view.setViewName("ib_revoke");

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userName = auth.getName();
        Map<String, List<TsmpScope>> tsmpScopeMap = tsmpApprovalService.getApprovedByUserId(userName);
        Set<String> clientIds = tsmpScopeMap.keySet();
        List<TsmpClient> tsmpClients = nodeArtSupporter.getClientByIds(clientIds);
        Map<String, String> tsmpClientMap = tsmpClients.stream().collect(Collectors.toMap(x -> x.getId(), x -> x.getAlias() == null?"":x.getAlias()));
        tsmpScopeMap.forEach((k, v) -> v.forEach(
                tsmpScope -> {
                    tsmpScope.setExpireDate(expireDate(tsmpScope.getAllowDays(), tsmpScope.getLastUpdateAt()));
                    tsmpScope.setAllowDays(secToDay(tsmpScope.getAllowDays()));
                }
        ));

        view.addObject("tsmpClientMap", tsmpClientMap);
        view.addObject("tsmpScopeMap", tsmpScopeMap);
        return view;
    }

    @PostMapping("/revoke")
    public String revoke(@RequestParam("clientIds") List<String> clientIds){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userName = auth.getName();
        authorizationRevoke.revoked(userName, clientIds);
        return "revoked";
    }



    //計算到期日
    private String expireDate(int allowDays, Date lastUpdateDate) {
        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(lastUpdateDate);
        rightNow.add(Calendar.SECOND, allowDays);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(rightNow.getTime());
    }

    //秒數轉天數
    private int secToDay(int sec) {
        return sec / 86400;
    }
}
