package com.authcode.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 訪問/login時返回自訂登入頁面
 */
@Controller
public class LoginController {
    /* [static] field */

    /* [static] */

    /* [static] method */

    /* [instance] field */

    /* [instance] constructor */

    /* [instance] method */
    @RequestMapping("/login")
    public String login(){
        return "login";
    }
    
    @RequestMapping("/pre-login")
    public String preLogin(){
        return "pre-login";
    }

    /* [instance] getter/setter */
}
