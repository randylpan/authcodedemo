package com.authcode.demo.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {
    /* [static] field */

    /* [static] */

    /* [static] method */

    /* [instance] field */

    /* [instance] constructor */

    /* [instance] method */
    @PostMapping("/send_otp")
    public String sendOTP(){
        //模擬 發送SMS OTP
        return "Send OTP Code:123456";
    }

    /* [instance] getter/setter */
}
