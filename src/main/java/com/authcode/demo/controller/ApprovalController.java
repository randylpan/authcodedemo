package com.authcode.demo.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.common.util.OAuth2Utils;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.DefaultCsrfToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.thinkpower.tsmp.apim.vo.TsmpAPI;
import com.thinkpower.tsmp.apim.vo.TsmpScope;
import com.thinkpower.tsmp.sdk.ifs.TsmpVirtualGroupService;
import com.thinkpower.tsmp.sdk.vo.TsmpVScope;
import com.thinkpower.tsmp.smd.ns.ifs.NodeArtSupporter;
import com.thinkpower.tsmp.smd.ns.ifs.vo.TsmpClient;
import com.thinkpower.tsmp.smd.ns.ifs.vo.TsmpClientGroupInfo;

/**
 * 訪問/oauth/confirm_access時返回自訂授權頁面
 */
@Controller
@SessionAttributes({ApprovalController.AUTHORIZATION_REQUEST_ATTR_NAME, ApprovalController.ORIGINAL_AUTHORIZATION_REQUEST_ATTR_NAME})
public class ApprovalController {
	
	static final String AUTHORIZATION_REQUEST_ATTR_NAME = "authorizationRequest";

	static final String ORIGINAL_AUTHORIZATION_REQUEST_ATTR_NAME = "org.springframework.security.oauth2.provider.endpoint.AuthorizationEndpoint.ORIGINAL_AUTHORIZATION_REQUEST";

    @Autowired
    private NodeArtSupporter nodeArtSupporter;

    @Autowired
    private TsmpVirtualGroupService tsmpVirtualGroupService;

    private final Logger logger = LoggerFactory.getLogger(getClass());


    @RequestMapping("/oauth/confirm_access")
    public ModelAndView getAccessConfirmation(Map<String, Object> model, HttpServletRequest request) throws Exception {
        AuthorizationRequest authorizationRequest = (AuthorizationRequest) model.get("authorizationRequest");

        String clientId = authorizationRequest.getClientId();

        ModelAndView view = new ModelAndView();

        //防止csrf 必要
        CsrfToken csrfToken = (CsrfToken) (model.containsKey("_csrf") ? model.get("_csrf") : request.getAttribute("_csrf"));

        TsmpClient tsmpClient = nodeArtSupporter.getClientById(clientId);
        String clientName = tsmpClient.getName();
        view.addObject("tsmpClient", tsmpClient);

        Map<String, String> expiryDateMap = new HashMap<>();
        Map<String, String> expiryDateMapV = new HashMap<>();
        boolean isOneTimeScope = false;
        //部署至TSMP時可取得TsmpScope物件
        if (model.containsKey("tsmpScopes") || request.getAttribute("tsmpScopes") != null) {
            Map<String, TsmpScope> tsmpScopes = (Map<String, TsmpScope>) request.getAttribute("tsmpScopes");            
            Map<String, TsmpVScope> tsmpVScopes = tsmpVirtualGroupService.getVGroupData(tsmpScopes);
            if (null != tsmpVScopes.get(TsmpVirtualGroupService.NOT_IN_VGROUP)) {
                tsmpScopes = tsmpVScopes.get(TsmpVirtualGroupService.NOT_IN_VGROUP).getTsmpScopeMap();
                for (String scopeId: tsmpScopes.keySet()) {
                    TsmpScope tsmpScope = tsmpScopes.get(scopeId);
                    if (clientName.equals(tsmpScope.getName())){
                        List<TsmpAPI> tsmpAPIS = tsmpScope.getTsmpAPIS();
                        List<TsmpAPI> filteredTsmpAPIS = new ArrayList<>();
                        for (TsmpAPI api:tsmpAPIS) {
                            if (!api.getApiName().startsWith("DPF")){
                                filteredTsmpAPIS.add(api);
                            }
                        }
                        tsmpScope.setTsmpAPIS(filteredTsmpAPIS);
                    }
                    expiryDateMap.put(scopeId, expireDate(tsmpScope.getAllowDays(), tsmpScope.getLastUpdateAt()));
                    tsmpScope.setAllowDays(secToDay(tsmpScope.getAllowDays()));
                }
                view.addObject("tsmpNVScope", tsmpScopes);
                tsmpVScopes.remove(TsmpVirtualGroupService.NOT_IN_VGROUP);
            } else {
                view.addObject("tsmpNVScope", new HashMap<String, TsmpScope>());
            }
            view.addObject("expireDate", expiryDateMap);

            for (String scopeId: tsmpVScopes.keySet()) {
                TsmpVScope tsmpVScope = tsmpVScopes.get(scopeId);
                expiryDateMapV.put(scopeId, expireDate(tsmpVScope.getAllowDays(), tsmpVScope.getLastUpdateAt()));
                tsmpVScope.setAllowDays(secToDay(tsmpVScope.getAllowDays()));
            }
            view.addObject("tsmpVScope", tsmpVScopes);
            view.addObject("expiryDateMapV", expiryDateMapV);
        }
        //判斷是否為第一次授權
        if ((model.containsKey("isFirstTimesApproval") || request.getAttribute("isFirstTimesApproval") != null) && !isOneTimeScope) {
            Boolean isFirstTimesApproval = (Boolean) request.getAttribute("isFirstTimesApproval");
            view.addObject("isFirstTimesApproval", isFirstTimesApproval);
        }
        //返回的頁面名
        view.setViewName("approval");
        //前端需要顯示哪些資料可如下設定
        view.addObject("_csrf", csrfToken);
        return view;
    }
    
    @RequestMapping("/check_input")
    public ModelAndView getOTP(@RequestParam Map<String, String> requestParameters, Map<String, Object> model, HttpServletRequest request) throws Exception {
    	String authType = requestParameters.get("auth_type");
    	String scope = requestParameters.get("scope");
    	if (authType == null || authType.isEmpty() || !authType.equalsIgnoreCase("smsotp") || scope.isEmpty()) {
    		return new ModelAndView("redirect:/oauth/authorize?error", model);
    	}
    	//用 GROUP_ID找出 ALLOW_TIMES
    	List<Object> scopes = new ArrayList<>();
    	scopes.add(scope);
    	List<TsmpClientGroupInfo> groupInfoList = nodeArtSupporter.getTClientGroup(scopes);
    	if (groupInfoList == null || groupInfoList.isEmpty()) {
    		return new ModelAndView("redirect:/oauth/authorize?error", model);
    	}
    	int allowTRimes = groupInfoList.get(0).getAllowAccessUseTimes();
    	if (allowTRimes != 1) {
    		return new ModelAndView("redirect:/oauth/authorize?error", model);
    	}
    	
    	//TODO
        //界接 SMS OTP server 取得 OTP用於後續驗證用
    	
    	ModelAndView view = new ModelAndView();
        //防止csrf 必要
    	CsrfToken csrfToken = new DefaultCsrfToken("X-CSRF-TOKEN", "_csrf", requestParameters.get("_csrf"));
		view.setViewName("otp_input");
        //前端需要顯示哪些資料可如下設定
        view.addObject("_csrf", csrfToken);
        Map<String, String> approvalParameters = new HashMap<String, String>();
        approvalParameters.put("user_oauth_approval", "true");
        approvalParameters.put("_csrf", csrfToken.getToken());
        approvalParameters.put(scope, "true");
        approvalParameters.put("authorize", "Authorize");
        approvalParameters.put("ib", requestParameters.get("ib"));
        approvalParameters.put("password", requestParameters.get("password"));
        approvalParameters.put("phoneNum", requestParameters.get("phoneNum"));
        view.addObject("approvalParameters", approvalParameters);
        
        //generate authorization_request
        AuthorizationRequest authorizationRequest = createAuthorizationRequest(requestParameters);
        authorizationRequest.setApprovalParameters(approvalParameters);       
        model.put(AUTHORIZATION_REQUEST_ATTR_NAME, authorizationRequest);
		model.put(ORIGINAL_AUTHORIZATION_REQUEST_ATTR_NAME, unmodifiableMap(authorizationRequest));
    	return view;
    }
    
    @PostMapping(value = "/otp_input")
	public String otpInput() {
		return "otp_input";
	}

    //計算到期日
    private String expireDate(int allowDays, Date lastUpdateDate){
        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(lastUpdateDate);
        rightNow.add(Calendar.SECOND, allowDays);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(rightNow.getTime());
    }

    //秒數轉天數
    private int secToDay(int sec){
        return sec / 86400;
    }
    
    private AuthorizationRequest createAuthorizationRequest(Map<String, String> authorizationParameters) {

		String clientId = authorizationParameters.get(OAuth2Utils.CLIENT_ID);
		String state = authorizationParameters.get(OAuth2Utils.STATE);
		String redirectUri = authorizationParameters.get(OAuth2Utils.REDIRECT_URI);
		Set<String> responseTypes = OAuth2Utils.parseParameterList(authorizationParameters
				.get(OAuth2Utils.RESPONSE_TYPE));
		Set<String> scopes = OAuth2Utils.parseParameterList(authorizationParameters.get(OAuth2Utils.SCOPE));
		
		AuthorizationRequest request = new AuthorizationRequest(authorizationParameters,
				Collections.<String, String> emptyMap(), clientId, scopes, null, null, false, state, redirectUri,
				responseTypes);

		return request;

	}
    
    Map<String, Object> unmodifiableMap(AuthorizationRequest authorizationRequest) {
		Map<String, Object> authorizationRequestMap = new HashMap<String, Object>();

		authorizationRequestMap.put(OAuth2Utils.CLIENT_ID, authorizationRequest.getClientId());
		authorizationRequestMap.put(OAuth2Utils.STATE, authorizationRequest.getState());
		authorizationRequestMap.put(OAuth2Utils.REDIRECT_URI, authorizationRequest.getRedirectUri());
		if (authorizationRequest.getResponseTypes() != null) {
			authorizationRequestMap.put(OAuth2Utils.RESPONSE_TYPE,
					Collections.unmodifiableSet(new HashSet<String>(authorizationRequest.getResponseTypes())));
		}
		if (authorizationRequest.getScope() != null) {
			authorizationRequestMap.put(OAuth2Utils.SCOPE,
					Collections.unmodifiableSet(new HashSet<String>(authorizationRequest.getScope())));
		}
		authorizationRequestMap.put("approved", authorizationRequest.isApproved());
		if (authorizationRequest.getResourceIds() != null) {
			authorizationRequestMap.put("resourceIds",
					Collections.unmodifiableSet(new HashSet<String>(authorizationRequest.getResourceIds())));
		}
		if (authorizationRequest.getAuthorities() != null) {
			authorizationRequestMap.put("authorities",
					Collections.unmodifiableSet(new HashSet<GrantedAuthority>(authorizationRequest.getAuthorities())));
		}

		return Collections.unmodifiableMap(authorizationRequestMap);
	}
}
