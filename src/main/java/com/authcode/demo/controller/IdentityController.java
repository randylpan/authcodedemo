package com.authcode.demo.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.common.util.OAuth2Utils;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.thinkpower.tsmp.apim.ifs.IdentityService;
import com.thinkpower.tsmp.apim.vo.TsmpIdentity;

@Controller
@SessionAttributes({ IdentityController.AUTHORIZATION_REQUEST_ATTR_NAME, "approvalParameters" })
public class IdentityController {
	/* [static] field */
	static final String AUTHORIZATION_REQUEST_ATTR_NAME = "authorizationRequest";

	/* [static] */

	/* [static] method */

	/* [instance] field */
	private final Logger logger = LoggerFactory.getLogger(getClass()); // slf4j

	@Autowired
	private IdentityService identityService;

	@Autowired
	public AuthenticationManager authenticationManagerBean;

	/* [instance] constructor */

	/* [instance] method */
	@PostMapping(value = "/identity", params = OAuth2Utils.USER_OAUTH_APPROVAL)
	public ModelAndView identity(@RequestParam Map<String, String> approvalParameters, Map<String, Object> model,
			HttpServletRequest request) {
		/** 從session取得AuthorizationRequest物件 */
		AuthorizationRequest authorizationRequest = (AuthorizationRequest) model.get(AUTHORIZATION_REQUEST_ATTR_NAME);

		List<String> scopeList = new ArrayList<>();
		// 取得所有scpoe id
		Set<String> scopeIds = authorizationRequest.getScope();
		// 判斷前端所點選的授權項目並塞進scopeList
		for (String scopeId : scopeIds) {
			if (approvalParameters.containsKey(scopeId)) {
				if ("true".equals(approvalParameters.get(scopeId))) {
					scopeList.add(scopeId);
				}
			}
		}
		if (CollectionUtils.isEmpty(scopeList)) {
			return new ModelAndView("forward:/oauth/authorize", model);
		}
		/**
		 * 將授權的scope id透過此方法取得核身方式 key=scopeId, value=核身類集合 Local端開發資料為hot code
		 * 可參考{@link com.thinkpower.tsmp.sdk.vi.IdentityService_SDKVirtual}
		 * 部署至TSMP後資料為DB設定資料
		 */
		Map<String, List<TsmpIdentity>> identityMap = identityService.getIdentityByScopeIds(scopeList);
		if (CollectionUtils.isEmpty(identityMap)) {
			return new ModelAndView("forward:/oauth/authorize", model);
		}

		/**
		 * 開發團隊實作核身判斷
		 * 此範例為先取每個scope對應的核身種類中等級最小(數字最大)集合,在取集合中的等級最大(數字最小),如核身種類名身有OTP即需要OTP驗證
		 **/
		Set<TsmpIdentity> identitySet = new HashSet<>();
		for (String identityId : identityMap.keySet()) {
			TsmpIdentity identity = getLargest(identityMap.get(identityId));
			identitySet.add(identity);
		}
		TsmpIdentity highLevelIdentity = getSmallest(identitySet);
		boolean isRedirectOTP = false;
		if (highLevelIdentity.getName().contains("OTP")) {
			isRedirectOTP = true;
		}
		/** 上面範例 如果isRedirectOTP為true 即導向otp.html **/
		if (isRedirectOTP) {
			// 將使用者點選的授權項目傳至otp.html 以hidden方式保留
			model.put("approvalParameters", approvalParameters);
			/**
			 * 部署至TSMP時 以此方式導向
			 */
			// String dcName = (String)
			// request.getAttribute("tsmp.request.process.dc.name");
			// String moduleName = (String)
			// request.getAttribute("tsmp.request.process.module.name");
			// ModelAndView modelAndView = new
			// ModelAndView("redirect:/"+dcName+"/"+moduleName+"/otp", model);

			ModelAndView modelAndView = new ModelAndView("redirect:/otp", model);
			return modelAndView;
		}
		// 如無需二次核身則以此方式導向取得授權碼
		return new ModelAndView("forward:/oauth/authorize", model);
	}

	@GetMapping(value = "/otp")
	public String otp() {
		return "otp";
	}

	@PostMapping(value = "/otpcheck", params = OAuth2Utils.USER_OAUTH_APPROVAL)
	public ModelAndView otpCheck(@RequestParam Map<String, String> otpParameter, Map<String, ?> model) {
		String otp = otpParameter.get("otp");
		// otp驗證
		if ("123456".equals(otp)) {
			return new ModelAndView("forward:/oauth/authorize", model);
		}
		// 如驗證失敗導向失敗頁面
		return new ModelAndView("redirect:/otp?error", model);
	}

	@PostMapping(value = "/otp_check", params = OAuth2Utils.USER_OAUTH_APPROVAL)
	public ModelAndView otpCheckSe(@RequestParam Map<String, String> otpParameter, Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView("forward:/oauth/authorize", model);
	}

	private TsmpIdentity getSmallest(Collection<TsmpIdentity> identityCollection) {
		Set<TsmpIdentity> sortSet = new TreeSet<>((o1, o2) -> {
			Integer level = Integer.parseInt(o1.getLevel());
			Integer level2 = Integer.parseInt(o2.getLevel());
			if (level > level2) {
				return 1;
			} else if (level < level2) {
				return -1;
			} else {
				return 0;
			}
		});
		sortSet.addAll(identityCollection);
		return getFirstTsmpIdentity(sortSet);
	}

	private TsmpIdentity getLargest(Collection<TsmpIdentity> identityCollection) {
		Set<TsmpIdentity> sortSet = new TreeSet<>((o1, o2) -> {
			Integer level = Integer.parseInt(o1.getLevel());
			Integer level2 = Integer.parseInt(o2.getLevel());
			if (level > level2) {
				return -1;
			} else if (level < level2) {
				return 1;
			} else {
				return 0;
			}
		});
		sortSet.addAll(identityCollection);
		return getFirstTsmpIdentity(sortSet);
	}

	private TsmpIdentity getFirstTsmpIdentity(Set<TsmpIdentity> sortSet) {
		Iterator<TsmpIdentity> identityIterator = sortSet.iterator();
		while (identityIterator.hasNext()) {
			return identityIterator.next();
		}
		return null;
	}

	/* [instance] getter/setter */
}
