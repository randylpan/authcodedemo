package com.authcode.demo.providers;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.authcode.demo.authentication.PreIBUsernamePasswordAuthenticationToken;

public class PreIBUserAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

	private UserDetailsService userDetailsService;

	/**
	 * 客製化第三方驗證
	 * 
	 * @param userDetails
	 * @param authentication
	 *            先前從filter客製的欄位物件
	 * @throws AuthenticationException
	 */
	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
		PreIBUsernamePasswordAuthenticationToken customAuthentication = (PreIBUsernamePasswordAuthenticationToken) authentication;
		String id = customAuthentication.getName();
		String password = (String) customAuthentication.getCredentials();
		String phoneNum = (String) customAuthentication.getPhoneNumber();
		// 界接SMSOTP server 驗證
		String otpCode = (String) customAuthentication.getOtpCode();
		if (otpCode.equals(otpCode)) {
			return;
		}
		throw new BadCredentialsException(
				messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "身份驗證失敗"));

	}

	/**
	 * 因為不存User資料,但spring security驗證過程中需要取得User資料所以模擬回傳一個假的User
	 * 
	 * @param username
	 * @param authentication
	 * @return return mock user object
	 * @throws AuthenticationException
	 */
	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
		return this.getUserDetailsService().loadUserByUsername(username);
	}

	/**
	 * 會依據哪種AuthenticationToken決定是否執行此provider
	 * 
	 * @param authentication
	 * @return
	 */
	@Override
	public boolean supports(Class<?> authentication) {
		return PreIBUsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}

	public UserDetailsService getUserDetailsService() {
		return userDetailsService;
	}

	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}
}
