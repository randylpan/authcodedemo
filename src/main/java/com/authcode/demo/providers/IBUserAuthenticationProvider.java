package com.authcode.demo.providers;

import com.authcode.demo.authentication.IBUsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public class IBUserAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    private UserDetailsService userDetailsService;
    /**
     * 客製化第三方驗證
     * @param userDetails
     * @param authentication 先前從filter客製的欄位物件
     * @throws AuthenticationException
     */
    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        String id = authentication.getName();
        String password = (String) authentication.getCredentials();
        if (id.equals("user") && password.equals("123456")){
            return;
        }
        if (id.equals("user2") && password.equals("123456")){
            return;
        }
        if (id.equals("user3") && password.equals("123456")){
            return;
        }
        throw new BadCredentialsException(messages.getMessage(
                "AbstractUserDetailsAuthenticationProvider.badCredentials",
                "身份驗證失敗"));

    }

    /**
     * 因為不存User資料,但spring security驗證過程中需要取得User資料所以模擬回傳一個假的User
     * @param username
     * @param authentication
     * @return return mock user object
     * @throws AuthenticationException
     */
    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        return this.getUserDetailsService().loadUserByUsername(username);
    }

    /**
     * 會依據哪種AuthenticationToken決定是否執行此provider
     * @param authentication
     * @return
     */
    @Override
    public boolean supports(Class<?> authentication) {
        return IBUsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }

    public UserDetailsService getUserDetailsService() {
        return userDetailsService;
    }

    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }
}
