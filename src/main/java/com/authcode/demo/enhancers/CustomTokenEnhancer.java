package com.authcode.demo.enhancers;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;

@Component
public class CustomTokenEnhancer implements TokenEnhancer {

    /* [static] field */

    /* [static] */

    /* [static] method */

    /* [instance] field */

    /* [instance] constructor */

    /* [instance] method */
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        //避免多個enhancer訊息被覆蓋 推薦以下方式建立additionalInfo
        Map<String, Object> additionalInfo = new LinkedHashMap<String, Object>(accessToken.getAdditionalInformation());
        additionalInfo.put("user_nid", "sha256/md5加密後字串");

        additionalInfo.put("CustomField", "Custom Message");

        DefaultOAuth2AccessToken defaultOAuth2AccessToken = (DefaultOAuth2AccessToken) accessToken;
        defaultOAuth2AccessToken.setAdditionalInformation(additionalInfo);
        return accessToken;
    }
    /* [instance] getter/setter */
}
